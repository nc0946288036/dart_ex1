
import 'dart:io';

void main() {
  // ---------------------------------------------------------------- //
  print("#5 Map – Pizza Ordering");
  // ---------------------------------------------------------------- //
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };
  List menu = const ['margherita', 'pepperoni', 'vegetarian'];
  List? order = [];
  var numMenu = 0, total = 0.0;

  print("Please select your menu below.\n"
      " 🍕 margherita\n"
      " 🍕 pepperoni\n"
      " 🍕 vegetarian\n");

  print("How many menu do you want to order?");
  numMenu = int.parse(stdin.readLineSync()!);

  List<bool> checkMenu = List<bool>.filled(numMenu, false);

  if (numMenu<=pizzaPrices.length) {

    print("Order Menu...");
    for (int i=0; i<numMenu; i++) {
      order.add(stdin.readLineSync());
    }

    for (var i=0; i<numMenu; i++) {
      checkMenu[i] = false;
      for (var j=0; j<pizzaPrices.length; j++) {
        if (order[i].compareTo(menu[j]) == 0) {
          checkMenu[i] = true;
          total += pizzaPrices[menu[j]]!;
        }
      }
    }

    for (var check=0; check<checkMenu.length; check++) {
      if (checkMenu[check] == false) {
        print('${order[check]} pizza is not on the menu.');
      }
    }
  } else {
    print("Menu is limited to ${pizzaPrices.length} items, please check your order again.");
  }
  print("Total : \$$total");
}
//6450110007